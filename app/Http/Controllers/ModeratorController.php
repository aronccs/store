<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class ModeratorController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('moderator');
    }

    public function index()
    {
        return view('moderator.index');
    }

    public function create()
    {
        return view('moderator.create');
    }

    public function store() {
        $this->validate(request(), [
            'name' => 'required|min:3|max:150',
            'description' => 'required|min:3|max:150',
            'category' => 'required'
        ]);

        $item = new Item;

        $item->name = request('name');
        $item->description = request('description');
        $item->category = request('category');        
        
        $item->save();

        return redirect('/moderator');
    }
}
