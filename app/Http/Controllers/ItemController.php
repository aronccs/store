<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Item;

class ItemController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }

    public function index(Request $request) 
    {
        if (!Auth::guest())
        {
            if ($request->user()->hasRole('admin'))
            {
                return redirect('/admin');
            }

            elseif ($request->user()->hasRole('moderator'))
            {
                return redirect('/moderator');
            }
        }

        return view('store.index');
    }

    public function items()
    {
        $items = Item::all();
        return view('store.items', compact('items'));
    }

    public function category($cat)
    {
        $items = Item::where('category', $cat)->get();
        return view('store.items', compact('items'));
    }
}
