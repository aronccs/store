<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function categories()
    {
        
    }
}
