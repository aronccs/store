<p class="text-center font-weight-bold">Categories</p>

<ul class="list-group list-group-flush">
   <a href="/items" class="list-group-item list-group-item-action {{ Request::is('items') ? 'list-group-item-dark' : '' }}">All</a>
   <a href="/items/electronics" class="list-group-item list-group-item-action {{ Request::is('items/electronics') ? 'list-group-item-dark' : '' }}">Electronics</a>
   <a href="/items/home_appliances" class="list-group-item list-group-item-action {{ Request::is('items/home_appliances') ? 'list-group-item-dark' : '' }}">Home Appliances</a>
   <a href="#" class="list-group-item list-group-item-action">Furnitures</a>
   <a href="#" class="list-group-item list-group-item-action">Fashion</a>
   <a href="#" class="list-group-item list-group-item-action">Beauty Products</a>
   <a href="#" class="list-group-item list-group-item-action">Miscellaneous</a>
</ul>