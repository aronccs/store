@extends ('layouts.moderator')

@section ('content')

    <h1>Add Item</h1>
    <hr>

    <form action="/moderator/store" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Item Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Item Name">           
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Description">
        </div>

        <input type="hidden" name="category" value="electronics">
        
        <button type="submit" class="btn btn-success">Submit</button>
    </form>

@endsection