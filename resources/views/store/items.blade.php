@extends('layouts.master')

@section('content')

    <h2 class="text-center">All Items</h2>
    <hr />

    @php 
        $i=1;
    @endphp

    @foreach($items as $item)

        {{-- Create a new row for every 3 items --}}

        @if($i == 1 || $i % 4 == 0)
            <div class="row">
                <div class="col-sm-4">
        @else
            <div class="col-sm-4">
        @endif


        <div class="card mb-4">
            <img class="card-img-top img-fluid" src="/images/pencil.jpg" alt="Image">
            <div class="card-body">
                <h5 class="card-title">{{ $item->name }}</h5>
                <p><small>{{ $item->created_at->toFormattedDateString() }}</small></p>
                <p class="card-text">{{ $item->description }}</p>
            </div>
        </div>
        

        @if($i % 3 == 0)
                </div> {{-- close col --}}
            </div> {{-- close row --}}            
        @else
            </div> {{-- close col --}}
        @endif

        @php
            $i++;
        @endphp

    @endforeach

@endsection