<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Store</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
</head>
<body style="padding-top: 40px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                @include('includes.admin.header')
            </div>
        </div>

        <br /><br />

        <div class="row">
            <div class="col-2">
                @include('includes.admin.sidebar-left')
            </div>

            <div class="col-8">
                @yield('content')
            </div>

            <div class="col-2">
                @include('includes.admin.sidebar-right')
            </div>
        </div>

        <div class="row">
            <div class="col">
                @include('includes.footer')
            </div>
        </div>
    </div>
</body>
</html>