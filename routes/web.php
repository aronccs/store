<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ItemController@index');

Route::get('/items', 'ItemController@items');
Route::get('/items/{category}', 'ItemController@category');

Auth::routes();

Route::get('/admin', 'AdminController@index');
Route::get('/admin/categories', 'AdminController@categories');

Route::get('/moderator', 'ModeratorController@index');
Route::get('/moderator/create', 'ModeratorController@create');
Route::post('/moderator/store', 'ModeratorController@store');