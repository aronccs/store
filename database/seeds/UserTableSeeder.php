<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_member = Role::where('name', 'member')->first();
        $role_moderator = Role::where('name', 'moderator')->first();
        $role_admin  = Role::where('name', 'admin')->first();
        
        $member = new User();
        $member->username = 'member';
        $member->email = 'member@example.com';
        $member->password = bcrypt('secret');
        $member->first_name = 'Member';
        $member->last_name = 'Name';
        $member->save();
        $member->roles()->attach($role_member);
        
        $moderator = new User();
        $moderator->username = 'moderator';
        $moderator->email = 'moderator@example.com';
        $moderator->password = bcrypt('secret');
        $moderator->first_name = 'Moderator';
        $moderator->last_name = 'Name';
        $moderator->save();
        $moderator->roles()->attach($role_moderator);

        $admin = new User();
        $admin->username = 'admin';
        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('secret');
        $admin->first_name = 'Admin';
        $admin->last_name = 'Name';
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
